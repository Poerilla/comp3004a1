package core;

import java.util.ArrayList;
import java.util.Collections;

public class Deck {
	
	private ArrayList<String> deck;
	
	public Deck() {
		deck = new ArrayList<String>();
		deck.add("H2");
		deck.add("H3");
		deck.add("H4");
		deck.add("H5");
		deck.add("H6");
		deck.add("H7");
		deck.add("H8");
		deck.add("H9");
		deck.add("H10");
		deck.add("HK");
		deck.add("HQ");
		deck.add("HJ");
		deck.add("HA");
		deck.add("C2");
		deck.add("C3");
		deck.add("C4");
		deck.add("C5");
		deck.add("C6");
		deck.add("C7");
		deck.add("C8");
		deck.add("C9");
		deck.add("C10");
		deck.add("CK");
		deck.add("CQ");
		deck.add("CJ");
		deck.add("CA");
		deck.add("S2");
		deck.add("S3");
		deck.add("S4");
		deck.add("S5");
		deck.add("S6");
		deck.add("S7");
		deck.add("S8");
		deck.add("S9");
		deck.add("S10");
		deck.add("SK");
		deck.add("SQ");
		deck.add("SJ");
		deck.add("SA");
		deck.add("D2");
		deck.add("D3");
		deck.add("D4");
		deck.add("D5");
		deck.add("D6");
		deck.add("D7");
		deck.add("D8");
		deck.add("D9");
		deck.add("D10");
		deck.add("DK");
		deck.add("DQ");
		deck.add("DJ");
		deck.add("DA");
		
	}
	
	public void shuffle() {
		Collections.shuffle(deck);
	}
	
	public String[] getDeck(){
		
		String[] array =  new String[deck.size()];
		for (int i = 0; i < deck.size(); i++) {
			array[i] = deck.get(i);
		}
		return array;
	}
}

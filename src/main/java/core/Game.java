package core;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Game {
	
	protected static String inputType = "none"; 
	protected static String fileInput = "none";
	protected static String consoleInput = "none";
	protected ArrayList<String> moves = new ArrayList<String>();
	Player user = new Player("user");
	Player dealer = new Player("dealer");
	
	public boolean turn = false; //false is the dealers turn

	public Game() {
//		String inputType = "none"; 
//		String fileInput = "none";
//		String consoleInput = "none";
	}
	/**
	 * Initialize game variables and determine the playing mode of the game*/
	public void init() {
		
		System.out.println("Type 'c' for console input and 'f' for file input: \n");
		Scanner scanner = new Scanner(System.in);
		inputType = scanner.nextLine();
		//System.out.println("Your inputType is " + inputType);
		scanner.close();
		
		
		
	}
	
	public void update(){
		
		if(turn) {
			//player the user
		}
		else {
			//play the dealer
			int dealerPoints = dealer.getPoints();
			if (dealerPoints <= 16) {
				//dealer.hit(card);
			} 
			else if (dealerPoints == 17 && dealer.getAces() >= 1) {
				//hit that soft seventeen
			}
			
		}
//		if (user.getPoints() > 21) {
//			return "User Bust";
//		} 
//		else if (dealer.getPoints() > 21) {
//			return "Dealer Bust";
//		}
//		

		
		
	}
	
	public void hit(String type) {
		
		
		if (type.equals("user")) {
			user.hit(moves.remove(0));
		}else {
			dealer.hit(moves.remove(0));
		}
		
		user.calculatePoints();
		dealer.calculatePoints();
	}

	
	/**
	 * Read Data from a file
	 * @throws IOException */
	public static void readFromFile() throws IOException {
			
			
		  String OS = System.getProperty("os.name").toLowerCase();
		  File file1;
		  System.out.println(OS);
		  if (OS.equals("linux")) {
			  file1 = new File("/home/poe/git/comp3004a1/src/main/resources/file1.txt");
		  } else {
			  file1 = new File("C:\\Users\\POE\\git\\comp3004a1\\src\\main\\resources\\file1.txt"); 
			  
		  }
		  FileReader reader1 = null;
			try {
				reader1 = new FileReader(file1);
			} catch (FileNotFoundException e) {
				
				e.printStackTrace();
			}
		  BufferedReader br1 = new BufferedReader(reader1); 
		  
		  String st; 
		  String result = "";
		  while ((st = br1.readLine()) != null) {
			    result += st; 
		} 
		  
		setFileInput(result);
		
	}
	
	/*
	 * Read from Console**/
	
	public static void readFromConsole () {
		
		System.out.println("Enter game input: \n");
		Scanner scanner = new Scanner(System.in);
		consoleInput = scanner.nextLine();
		System.out.println("Your inputType is " + consoleInput);
		scanner.close();
		
		setFileInput(consoleInput);
	}
	
	public void createCardArray(){
		String[] array = getFileInput().split(" ");
		for (int i = 0; i < array.length ; i++){
			moves.add(array[i]);
		}
		
	}
	
	/**
	 * The return array was strictly for testing purposes*/
	public String[] getCardArray(){
		String[] array = getFileInput().split(" ");
		for (int i = 0; i < array.length ; i++){
			moves.add(array[i]);
		}
		return array;
	}
	
	/**
	 * Getter method for input type which determines playing mode*/
	@SuppressWarnings("static-access")
	public String getInputType() {
		
		return this.inputType;
		
	}
	
	/**
	 * Getter method for input type which determines playing mode*/
	public String getFileInput() {
		return fileInput;
	}
	/*
	 * Setter method for input type which determines playing mode*/
	public static void setFileInput(String s) {
		fileInput = s;
	}
	/*
	 * String: Getter method for input type which determines playing mode*/
	public String getConsoleInput() {
		return consoleInput;
	}
	/*
	 * Integer: get the number of points the dealer has*/
	public int getDealerPoints() {
		
		return dealer.getPoints();
	}
	
	/*
	 * Void: Hit the dealer*/
	public void hitDealer(String card) {
		
		dealer.hit(card);
		
	}
	
}

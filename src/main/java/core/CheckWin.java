package core;
	
public class CheckWin {
	
	private Player user;
	private Player dealer;
	
	public CheckWin(Player user, Player dealer) {
		this.user = user;
		this.dealer = dealer;
	}
	
	public String check() {
		
		if (dealer.bust()) {
			return user.getType();
		} else if(dealer.blackjack() | (dealer.blackjack() && user.blackjack())) {
			return dealer.getType();
		} else if (user.bust()) {
			return dealer.getType();
		} else if (user.blackjack() && !dealer.blackjack()) {
			return user.getType();
		} else if (user.getStand() && dealer.getStand()) {
			if(user.getPoints() <= dealer.getPoints()) {
				return dealer.getType();
			} else {
				return user.getType();
			}
		}
		
		return null;
	}
}

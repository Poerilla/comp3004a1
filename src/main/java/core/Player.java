package core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Player {
	
	private ArrayList<String> cards =  new ArrayList<String>();
	private int points = 0;
	private String type = "";
	private int numAces = 0;
	private boolean bust = false;
	private boolean blackjack = false;
	private boolean stand;
	
	Set<Integer> numberedSet = new HashSet<Integer>();
	Set<String> alphaSet = new HashSet<String>();
	
	
	
	public Player(String type) {
		for(int i = 2; i< 10; i++)
				numberedSet.add(i);
		alphaSet.add("H");
		alphaSet.add("Q");
		alphaSet.add("J");
		alphaSet.add("K");
		
		this.setType(type);
	}
	
	public Player() {
		for(int i = 2; i< 10; i++)
				numberedSet.add(i);
		alphaSet.add("H");
		alphaSet.add("Q");
		alphaSet.add("J");
		alphaSet.add("K");
		
	}
	
	
	/**
	 * Pass in a string as a card value
	 * */
	public void hit(String card){
		
		if (this.getType().equals("dealer") && this.getPoints() > 16 && this.getAces() == 0) {
			
		} else if (this.getType().equals("dealer") && this.blackjack()){
			this.stand();
		} else {
			this.cards.add(card);
		}
		this.calculatePoints();
		
		//show one or all the cards
		if(this.getType().equals("dealer")) {
			if(this.bust() | this.blackjack()) {
				System.out.println("Dealer: " + Arrays.deepToString(this.cards.toArray()));
			}else {
				System.out.println("Dealer: " + cards.get(0));
			}
		}
		
		if(this.getType().equals("user"))  {
			System.out.println("User: " + Arrays.deepToString(this.cards.toArray()));
		}

	
	}
	
	/**
	 * Calculate the number of point a player has*/
	public void calculatePoints(){
		//TODO implement method to calculate the number of points a player has
		//Keep a count of the number of points
		int count = 0;
		int aceCount = 0;
		
		//Loop through and determine the points ignoring the aces for the time being
		for (int i = 0; i < cards.size();i++){
			String card = cards.get(i);
			String second =  Character.toString(card.charAt(1));
			if(alphaSet.contains(second)) {
				count += 10;
				continue;
			}
			else if(second.equals("A")){
				aceCount += 1;
				continue;
			}
			else if(numberedSet.contains(Integer.parseInt(second))){
				int num = Integer.parseInt(second);
				count += num;
				
			} else {
				count += 10;
			}

		}
		
	//	for (int i = 0; i < aceCount; i++  ){
			if(count + 12  < 22 && aceCount == 2){
				count += 12;
			}
			else if(count + 13 < 22 && aceCount == 3){
				count += 13;
			}
			else if (count + 11 < 22 && aceCount == 1){
				count += 11;
			}
			else if (count + 11 > 22 && aceCount == 1){
				count +=1;
			}
			else if (count + 14 < 22 && aceCount == 4){
				count +=14;
			}
			else if (count + 14 > 22 && aceCount == 4){
				count +=4;
			}
			else  {
				count += aceCount;
			}
			
		//}
		numAces =  aceCount;

		points = count;
		if(points > 21) {
			this.bust = true;
			System.out.println(this.getType() +" has busted");
		}
		if(points == 21) {
			this.blackjack = true;
			System.out.println(this.getType() +" has blackjack");
			
		}
		
	}
	/**
	 * Returns the number of points*/
	public int getPoints(){
		
		return this.points;
	}
	
	public String[] getCards() {
		//cards = {"SK","Qh","CA"}
		String[] array =  new String[cards.size()];
		for (int i = 0; i < cards.size(); i++) {
			array[i] = cards.get(i);
		}
		return array;
		
	}
	
	public void stand(){
		this.stand = true;
		System.out.println(this.getType() +" has decided to stand");
	}
	
	public boolean getStand() {
		
		return this.stand;
	}

	public int getAces() {
		
		return this.numAces;
	}
	
	public boolean bust() {
		//System.out.println(this.getType() +" has busted");
		return this.bust;
	}
	
	public boolean blackjack() {
		//System.out.println(this.getType() +" has blackjack");
		return this.blackjack;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}

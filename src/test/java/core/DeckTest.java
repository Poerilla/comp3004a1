package core;

import junit.framework.TestCase;
import static org.junit.Assert.*;

public class DeckTest extends TestCase{

	
		@SuppressWarnings("restriction")
		public void testCardsEqualBeforeShuffle() {
			Deck deck = new Deck();
			String[] array = deck.getDeck();
			assertArrayEquals(array, deck.getDeck());
			
			}
		
		@SuppressWarnings("restriction")
		public void testCardsNotEqualAfterShuffle() {
			Deck deck = new Deck();
			String[] array = deck.getDeck();
			deck.shuffle();
			assertArrayEquals(array, deck.getDeck());
			
			}
		
		@SuppressWarnings("restriction")
		public void testCountCards() {
			Deck deck = new Deck();
			
			deck.shuffle();
			assertEquals(52, deck.getDeck().length);
			
			}
		
}

package core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;
/**
 * Test that the player can make all the moves as per the assingment 
 * specification*/
public class PlayerMoveTest extends TestCase{

    
	/**
	 * Test that the player can hit at least once*/
	public void testPlayerHit() {
		Player user = new Player();
		String[] cardsHit = {"SK"};
		user.hit("SK");
		assertArrayEquals(cardsHit, user.getCards());
		
	}
	
	/**
	 * Test that the player can hit repeatedly*/
	public void testPlayerHitRepeat() {
		Player user = new Player();
		String[] cardsHit = {"SK", "HA"};
		user.hit("SK");
		user.hit("HA");
		assertArrayEquals(cardsHit, user.getCards());
	}

	/**
	 * Test that the players hand is updated after every turn*/
	public void testPlayerUpdate() {
		Player user = new Player("user");
		String[] cardsHit1 = {"SK"};
		user.hit("SK");
		assertArrayEquals(cardsHit1, user.getCards());
		String[] cardsHit2 = {"SK", "HA"};
		user.hit("HA");
		assertArrayEquals(cardsHit2, user.getCards());
		String[] cardsHit3 = {"SK", "HA", "AA"};
		user.hit("AA");
		assertArrayEquals(cardsHit3, user.getCards());
	}
	
	/**
	 * Test that the player busts after having more then 21 points*/
	public void testPlayerBust(){
		Player user = new Player("user");
		user.hit("SK");
		user.hit("HA");
		user.hit("HQ");
		assertEquals(!true, user.bust());
	}
	
	/**
	 * Test that the player busts after having more then 21 points*/
	public void testPlayerStand(){
		Player user = new Player("user");
		user.hit("SK");
		user.hit("HA");
		user.stand();
		assertEquals(true, user.getStand());
	}
	
	/**
	 * Test that the player busts after having more then 21 points*/
	public void testPlayerBust2(){
		Player user = new Player("user");
		user.hit("SK");
		user.hit("HA");
		user.hit("HQ");
		user.hit("HQ");
		assertEquals(true, user.bust());
	}

}

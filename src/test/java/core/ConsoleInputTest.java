package core;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.TestCase;

public class ConsoleInputTest extends TestCase {

	/**
	 * Test that input type c for console works*/
	public void testReadFromConsole() {
		Game blackjack = new Game();
		blackjack.init();
		assertEquals("c", blackjack.getInputType());
	}
	
	@SuppressWarnings("static-access")
	public void testReadFromConsole2() {
		Game blackjack = new Game();
		//blackjack.init();
		blackjack.readFromConsole();
		assertEquals("SK HA HQ CA", blackjack.getConsoleInput());
	}

}

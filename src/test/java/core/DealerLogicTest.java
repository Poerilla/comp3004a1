package core;

import static org.junit.Assert.*;

import org.junit.Test;

public class DealerLogicTest {

	
	@SuppressWarnings("restriction")
	@Test
	public void testDealerHit() {

		Player dealer = new Player("dealer");;
		String[] cardsHit = {"HK"};
		dealer.hit("HK");
		assertArrayEquals(cardsHit, dealer.getCards());
	}
	
	@SuppressWarnings("restriction")
	@Test
	public void testDealerHitRepeat() {

		Player dealer = new Player("dealer");;
		String[] cardsHit = {"HK", "C6"};
		dealer.hit("HK");
		dealer.hit("C6");
		assertArrayEquals(cardsHit, dealer.getCards());
		
	}
	
	@SuppressWarnings("restriction")
	@Test
	public void testDealerHitRepeat2() {

		Player dealer = new Player("dealer");
		dealer.hit("HK");
		dealer.hit("C6");		
		assertEquals(16, dealer.getPoints());
		
	}
	
	@SuppressWarnings("restriction")
	@Test
	public void testDealerHit16() {
		Player dealer = new Player("dealer");
		String[] cardsHit = {"CA", "C6","C3"};
		dealer.hit("CA");
		dealer.hit("C6");
		dealer.hit("C3");
		assertArrayEquals(cardsHit, dealer.getCards());
	}
	
	@SuppressWarnings("restriction")
	@Test
	public void testDealerSoft17() {
		Player dealer = new Player();
		@SuppressWarnings("unused")
		String[] cardsHit = {"HA", "C3", "D3"};
		dealer.hit("HA");
		dealer.hit("C3");
		dealer.hit("D3"); 
		assertEquals(17, dealer.getPoints());
		//assertEquals(true,dealer.bust());
	}
	@SuppressWarnings("restriction")
	@Test
	public void testDealerSoftAgain() {
		Player dealer = new Player();
		@SuppressWarnings("unused")
		String[] cardsHit = {"HA", "C3", "D3"};
		dealer.hit("HA");
		dealer.hit("C3");
		dealer.hit("D3"); 
		dealer.hit("H5");
		assertEquals(12, dealer.getPoints());
		//assertEquals(true,dealer.bust());
	}
	
	@Test
	@SuppressWarnings("restriction")
	public void testDealerSoftOver() {
		Player dealer = new Player("dealer");
		dealer.hit("HA");
		dealer.hit("C3");
		dealer.hit("D3"); 
		dealer.hit("H5");
		dealer.hit("HK");
		assertEquals(12, dealer.getPoints());
		//assertEquals(true,dealer.bust());
	}
	
	@SuppressWarnings("restriction")
	@Test
	public void testDealerBust() {
		Player dealer = new Player();
		@SuppressWarnings("unused")
		String[] cardsHit = {"HK", "HK", "HK"};
		dealer.hit("HK");
		dealer.hit("HK");
		dealer.hit("HK"); 
		//assertEquals(30, dealer.getPoints());
		assertEquals(true,dealer.bust());
	}

}

package core;

import static org.junit.Assert.*;

import org.junit.Test;

public class playerScoreTest {

	@SuppressWarnings("restriction")
	@Test
	public void calculateScore1() {
		Player user = new Player("user");
		user.hit("HJ");
		user.calculatePoints();
		assertEquals(10, user.getPoints());
	}
	@SuppressWarnings("restriction")
	@Test
	public void calculateScoreKing() {
		Player user = new Player("user");
		user.hit("HK");
		user.calculatePoints();
		assertEquals(10, user.getPoints());
	}
	
	@SuppressWarnings("restriction")
	@Test
	public void calculateScoreQueen() {
		Player user = new Player("user");
		user.hit("HQ");
		user.calculatePoints();
		assertEquals(10, user.getPoints());
	}
	
	@SuppressWarnings("restriction")
	@Test
	public void calculateScoreTen() {
		Player user = new Player("user");
		user.hit("H10");
		user.calculatePoints();
		assertEquals(10, user.getPoints());
	}
	
	@Test
	public void calculateScoreBlackJack() {
		Player user = new Player("user");
		user.hit("HK");
		user.hit("HA");
		user.calculatePoints();
		assertEquals(21, user.getPoints());
	}
	
	@Test
	public void calculateScoreOneAce() {
		Player user = new Player("user");
		user.hit("HK");
		user.hit("HJ");
		user.hit("CA");
		user.calculatePoints();
		assertEquals(21, user.getPoints());
	}
	
	@Test
	public void calculateScoreFourAces() {
		Player user = new Player("user");
		user.hit("HK");
		user.hit("SA");
		user.hit("CA");
		user.hit("HA");
		user.hit("DA");
		user.calculatePoints();
		assertEquals(14, user.getPoints());
	}
	
	@Test
	public void calculateScoreAceAsOne() {
		Player user = new Player("user");
		user.hit("HK");
		user.hit("S9");
		user.hit("SA");
		user.calculatePoints();
		assertEquals(20, user.getPoints());
	}
	
	@Test
	public void calculateScoreMultipleAces() {
		Player user = new Player("user");
		user.hit("HK");
		user.hit("S7");
		user.hit("CA");
		user.hit("H5");
		user.hit("DA");
		user.calculatePoints();
		assertEquals(24, user.getPoints());
	}

}

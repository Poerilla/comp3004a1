package core;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.extensions.TestSetup;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.*;

public class ReadFileTest extends TestCase{
	

	public void testReadFromFile() {
		Game blackjack = new Game();
		blackjack.init();
		assertEquals("none", blackjack.getFileInput());
	}
	/**
	 * Test string values are equal*/
	public void testReadFromFile2() {
		Game blackjack = new Game();
		try {
			blackjack.readFromFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals("SK HA HQ CA", blackjack.getFileInput());
	}
	/**
	 * Test array values are equal*/
	public void testReadFromFile3() {
		String[] elements = {"SK", "HA", "HQ", "CA"};
		Game blackjack = new Game();
		try {
			blackjack.readFromFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertArrayEquals(elements, blackjack.getCardArray());
	}
}

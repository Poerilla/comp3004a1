package core;

import static org.junit.Assert.*;

import org.junit.Test;

public class CheckWinTest {

	
	@SuppressWarnings("restriction")
	@Test
	public void testNoWin() {

		Player dealer = new Player("dealer");
		dealer.hit("HK");
		dealer.hit("D4");
		
		Player user = new Player("user");
		user.hit("SA");
		user.hit("C8");
		
		CheckWin checker = new CheckWin(user, dealer);
		assertEquals(null, checker.check());
	}
	
	@Test
	@SuppressWarnings("restriction")
	public void testWinDealer() {

		Player dealer = new Player("dealer");
		dealer.hit("HK");
		dealer.hit("HA");
		
		Player user = new Player("user");
		user.hit("CA");
		user.hit("C9");
		
		CheckWin checker = new CheckWin(user, dealer);
		assertEquals("dealer", checker.check());
	}
	
	@Test
	@SuppressWarnings("restriction")
	public void testWinUser() {

		Player dealer = new Player("dealer");
		dealer.hit("HK");
		dealer.hit("H9");
		
		Player user = new Player("user");
		user.hit("CA");
		user.hit("CK");
		
		CheckWin checker = new CheckWin(user, dealer);
		assertEquals("user", checker.check());
	}
	
	@Test
	@SuppressWarnings("restriction")
	public void testBothBlackJack() {

		Player dealer = new Player("dealer");
		dealer.hit("HK");
		dealer.hit("HA");
		
		Player user = new Player("user");
		user.hit("CA");
		user.hit("CK");
		
		CheckWin checker = new CheckWin(user, dealer);
		assertEquals("dealer", checker.check());
	}
	
	@Test
	@SuppressWarnings("restriction")
	public void testNoBlackJack() {

		Player dealer = new Player("dealer");
		dealer.hit("HK");
		dealer.hit("HA");
		dealer.hit("H5");
		
		Player user = new Player("user");
		user.hit("C6");
		user.hit("CK");
		user.hit("C4");
		user.stand();
		
		CheckWin checker = new CheckWin(user, dealer);
		assertEquals("dealer", checker.check());
		System.out.println("the winnner is " + checker.check());
	}

}
